﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGameScript : MonoBehaviour
{
    public Text txt;
    // Start is called before the first frame update
    void Start()
    {
        if(GameManager.Instance.endgameReason == 1)
        {
            txt.text = "Congratulation, you Won with a score of: " + GameManager.Instance.score;
        }
        else
        {
            txt.text = "Your plane Collided, you got a score of: " + GameManager.Instance.score;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Back))
            LoadStart();


        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            EndGame();
    }

    private void LoadStart()
    {
        SceneManager.LoadScene("StartScreen");
    }

    private void EndGame()
    {
        Application.Quit(0);
    }
}
