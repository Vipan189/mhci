﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour
{

    public int goalNumber;
    public bool finGoal = false;

    void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Starting");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {

            if (col.gameObject.tag.Equals("AircraftJet"))
            {
                print(col.gameObject.tag);
            }
            GameManager.Instance.flyThrough(goalNumber);
        if (finGoal)
        {
            GameManager.Instance.goalReached();
        }
    }

}
