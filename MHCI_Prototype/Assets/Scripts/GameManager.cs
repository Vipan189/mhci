﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : Singleton<GameManager>
{

    public Transform goals;
    public GameObject Trees;
    public Transform plane;
    public GameObject planePlayer;
    private int numberOfGoals;
    private int[] finGoals;
    public int score = 0;
    public Text text;
    public int time;
    public Text scoreT;
    UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController planeVars;
    private List<int> endlessgoals = new List<int>();
    public GameObject goalPrefab;
    public int endgameReason = 0;

    private void Awake()
    {
        //Endless Mode
        if(StaticClass.gameMode == 1)
        {
            print("Endless-Mode Active");
            Destroy(goals.gameObject);
            Destroy(Trees);
            Instantiate(goalPrefab, new Vector3(8f,0f,43), Quaternion.identity);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        planeVars = planePlayer.GetComponent<UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController>();
        planeVars.ForwardSpeed = 10;
        planeVars.Throttle = 10;
        
        
        print("Start GameManager");
        foreach(Transform go in goals)
        {
            numberOfGoals++;
        }
        print(numberOfGoals);
        finGoals = new int[numberOfGoals];
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.time);
        bool leftHanded = false;
        leftHanded = OVRInput.GetControllerPositionTracked(OVRInput.Controller.LTouch);

        OVRInput.Controller c = leftHanded ? OVRInput.Controller.LTouch : OVRInput.Controller.RTouch;

        if (OVRInput.GetControllerPositionTracked(c))
        {
            plane.localRotation = OVRInput.GetLocalControllerRotation(c);
            //plane.localPosition = OVRInput.GetLocalControllerPosition(c);
        }

        text.text = Time.time.ToString();
        scoreT.text = "Score: " + score.ToString();
        
        


    }

    public void flyThrough(int goalNumber)
    {
        print(goalNumber);
        if (StaticClass.gameMode == 0 && finGoals[goalNumber] != 1)
        {
            finGoals[goalNumber] = 1;
            score++;
        }

        if(StaticClass.gameMode == 1 && !endlessgoals.Contains(goalNumber+1))
        {
            endlessgoals.Add(goalNumber);
            score++;

            //Add new Goal in front of player
            Vector3 playerPos = plane.transform.position;
            Vector3 playerDirection = plane.transform.forward;
            Quaternion playerRotation = plane.transform.rotation;
            float spawnDistance = 100;
            //Testwise for a little bit of randomness 
            //float deviation = 0.4f;
            //float rand = Random.RandomRange(-1, 1)*deviation;
            //playerDirection = new Vector3(playerDirection.x + rand, playerDirection.y, playerDirection.z + rand);
            Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

            //Instantiate Goal and give it the next number as goal
            GameObject goal =  Instantiate(goalPrefab, new Vector3(spawnPos.x,0,spawnPos.z), playerRotation);
            goal.GetComponent<GoalScript>().goalNumber = score;
        }

        
    }

    public void planeCollided()
    {
        //Do Something nice and fancy
        Destroy(plane.gameObject);
        Destroy(planePlayer);
        Invoke("LoadEngameScene", 5);
    }

    public void goalReached()
    {
        score = 0;
        //Game is finished with last goal
        //Sow something or so
        foreach( int i in finGoals)
        {
            score += i;
        }
        endgameReason = 1;
        SceneManager.LoadScene("End_Screen", LoadSceneMode.Single);
        //LoadEnd-Scene
    }

    private void LoadEngameScene()
    {
        SceneManager.LoadScene("End_Screen", LoadSceneMode.Additive);
    }
}
