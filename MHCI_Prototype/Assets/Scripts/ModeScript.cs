﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ModeScript : MonoBehaviour
{

    public int gameType;
    public Button classic;
    public Button endless;
    // Start is called before the first frame update
    void Start()
    {
        classic.onClick.AddListener(startClassicMode);
        endless.onClick.AddListener(startEndlessMode);
    }

    // Update is called once per frame
    void Update()
    {
        bool leftHanded = false;
        leftHanded = OVRInput.GetControllerPositionTracked(OVRInput.Controller.LTouch);

        OVRInput.Controller c = leftHanded ? OVRInput.Controller.LTouch : OVRInput.Controller.RTouch;

        if (OVRInput.GetDown(OVRInput.Button.Back))
            startClassicMode();


        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            startEndlessMode();

    }



    public void setGameType(int i)
    {
        gameType = i;
    }

    private void startClassicMode()
    {
        StaticClass.gameMode = 0;
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }

    private void startEndlessMode()
    {
        StaticClass.gameMode = 1;
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }
}
